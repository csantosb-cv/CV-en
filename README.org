* [[https://csantosb-cv.gitlab.io/CV-en][CV (English)]]

[[https://gitlab.com/csantosb-cv/CV-en/-/commits/master][https://gitlab.com/csantosb-cv/CV-en/badges/master/pipeline.svg]]

CV based on a markup (org) / pandow [[https://csantosb-blogs.gitlab.io/blog-tech/posts/cv-in-org-mode/][workflow]].

Original references:

- Idea from this [[http://irreal.org/blog/?p=2815][article @ irreal blog]]
- Based on these examples by [[http://blog.chmd.fr/editing-a-cv-in-markdown-with-pandoc.html][Christophe]] and [[https://mszep.github.io/pandoc_resume/][Johnny]]

Except I am using Org instead of markdown.

** Files

- CV_*.org :: simple and extended source code versions of the cv

- .CV*.el  :: elisp config files, these are read and loaded along with its corresponding org files;
	      currently, they set the spell language to french

- Makefile :: exactly that

- *.tex and *.css :: latex and css forms for styles (see references)

- *.sh :: bash scripts for updating server with the local, latest release

** How this works

Quite simple. After updating the source =*.org= files, I just do a

#+begin_src sh
  make -k all
#+end_src

to build up the html, pdf, etc. under the public directory. I automatize the
process using [[https://gitlab.com/csantosb-cv/CV-en/pipelines][gitlab-ci]].

The project is [[https://gitlab.com/csantosb-cv][hosted]] and rendered using [[https://about.gitlab.com/features/pages/][gitlab pages]]. Additionally, I [[https://csantosb.pw/gitea/csantosb-cv][host]] a
copy in my own server. The resulting CV is [[https://csantosb-cv.gitlab.io/CV-en][here]].

** License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the [[http://www.gnu.org/licenses/gpl.txt][GNU General Public License]]
along with this program. If not, see http://www.gnu.org/licenses/gpl.html.
