all: pdf html docx odt

cv_en.html: CV_Cayetano_Santos_en.org style.css
	pandoc --standalone -c style.css --from org --to html -o public/cv_en.html CV_Cayetano_Santos_en.org

cv_en.pdf: cv_en.html
	wkhtmltopdf public/cv_en.html public/cv_en.pdf

docx: CV_Cayetano_Santos_en.org CV_Cayetano_Santos_en_detaille.org
	pandoc --from org --to docx -o public/CV_Cayetano_Santos_en.docx CV_Cayetano_Santos_en.org
	pandoc --from org --to docx -o public/CV_Cayetano_Santos_en_detaille.docx CV_Cayetano_Santos_en_detaille.org

odt: CV_Cayetano_Santos_en.org CV_Cayetano_Santos_en_detaille.org
	pandoc --from org --to odt -o public/CV_Cayetano_Santos_en.odt CV_Cayetano_Santos_en.org
	pandoc --from org --to odt -o public/CV_Cayetano_Santos_en_detaille.odt CV_Cayetano_Santos_en_detaille.org

cv_en.txt: cv_en.org
	pandoc --standalone --smart --from org --to plain -o public/cv_en.txt cv_en.org

pdf: style_green.tex CV_Cayetano_Santos_en.org
	pandoc --standalone --template style_green.tex \
	--from org --to context \
	-V papersize=A4 \
	-o public/CV_Cayetano_Santos_en.tex CV_Cayetano_Santos_en.org
	pandoc --standalone --template style_green.tex \
	--from org --to context \
	-V papersize=A4 \
	-o public/CV_Cayetano_Santos_en_detaille.tex CV_Cayetano_Santos_en_detaille.org
	cd public; context CV_Cayetano_Santos_en.tex
	cd public; context CV_Cayetano_Santos_en_detaille.tex

tox: tox-ID.org
	pandoc --standalone -c style.css --from org --to html -o public/tox-ID.html tox-ID.org

html: style_green.css CV_Cayetano_Santos_en.org CV_Cayetano_Santos_en_detaille.org
	pandoc --standalone -H style_green.css \
	--from org --to html \
	-o public/CV_Cayetano_Santos_en.html CV_Cayetano_Santos_en.org
	pandoc --standalone -H style_green.css \
	--from org --to html \
	-o public/CV_Cayetano_Santos_en_detaille.html CV_Cayetano_Santos_en_detaille.org

clean:
	cd public; rm -f CV*.tex *.tuc *.log

clean_all:
	cd public; rm -f CV*.html *.pdf *.docx *.txt CV*.tex *.tuc *.log *.odt
