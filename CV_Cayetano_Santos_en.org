* Cayetano Santos

#+begin_quote
  Physical Sciences - Master in Embedded Electronics\\
#+end_quote

\\

** Professional Experience

- 2019 :: *Senior VHDL design engineer,* (Indra Sistemas, Madrid)

- 2015,2018 :: *[[http://metiersit.dsi.cnrs.fr/index.php?page=ficheemploitype&codeEmploi=C1C44&idFamillePro=12&codeBAP=C&codeFamille=C][Research engineer]]*, /Electronics Department/ ([[http://www.apc.univ-paris7.fr][CNRS/APC]], Paris)

  + Head of department, with an increasing team management role within a
    specialized technical group. Position involving project monitoring, framing
    of activities, budget management, active participation to the laboratory
    instances, etc.

  + Senior designer and local technical manager for [[http://juno.ihep.cas.cn/][JUNO]] and [[http://www.dunescience.org/][DUNE]] experiences

    + [[http://www.apc.univ-paris7.fr/APC_CS/fr/juno][JUNO]], local coordinator, [[https://catiroc-test.pages.in2p3.fr/gui][software]] and [[https://catiroc-test.pages.in2p3.fr/firmware/][firmware]] developper. Test-bench
      characterisation, implementation and [[https://drive.google.com/open?id=14TkJYFLfEkf2CYFhtwJQ8O6PzHLJ3JIX][implémentâtion]] of [[http://iopscience.iop.org/article/10.1088/1748-0221/12/03/C03041][asic]]. Hardware
      [[https://indico.in2p3.fr/event/16933/contributions/60599/][developement]] à très forte [[https://drive.google.com/open?id=1P9ijX2cgEiitvCWrZ79RLPdWHklvXwv7][intégration]].

    + [[http://www.apc.univ-paris7.fr/APC_CS/en/wa105dune][DUNE]], local project manager and head senior designer for a COTS hardware
      platform based on Stratix IV / Nios II technology. Hardware [[https://drive.google.com/open?id=1cWpp7oBIyAYD_CeCmp5uu6hIzoFP7Xer][developments]]
      in [[http://www.nateurope.com/products/technologies/technologies_mtca.html][uTCA]] standard.

- 2011,2015 :: *Senior VHDL/FPGA designer*, /Nuclear Innovation Group/ ([[http://www.ciemat.es][CIEMAT]], Madrid)

  + Conception and [[https://drive.google.com/open?id=0B5n0QFi1Si4LNm9JRlYzMHk1aHc][development]] of a data acquisition system, high performing and
    [[https://drive.google.com/open?id=0B5n0QFi1Si4LcW1fWENsZzBuMVE][polyvalent]] (1 GHz, 12 bits on DDR3), aiming at instrumenting a large number
    of electronics channels corresponding to a [[https://drive.google.com/open?id=1lnw0R02P0FF31sqql5oT4DAwym1v_pOl][fast neutrons]] [[http://humania.org/2014/10/06/monster-el-espectrmetro-de-neutrones-diseado-por-el-ciemat-recibe-su-primer-haz/][detector]].
    Responsible for the firmware and software development: head VHDL firmware
    designer, as well as slow control and analysis software developer in
    MATLAB.

- 2008,2011 :: *Junior VHDL/FPGA designer*, /Groupe Capteurs CMOS/ ([[http://www.iphc.cnrs.fr/-PICSEL-.html][CNRS/IPHC]],
  Strasbourg)

  + Développement du code embarqué nécessaire au traitement en ligne des
    informations en provenance des pixels. Implémentation d’algorithmes
    spécifiques de traitement d’image sur des signaux issus des capteurs
    analogiques (matrices de pixels capables de délivrer des centaines d’images
    par seconde). [[https://drive.google.com/open?id=0B5n0QFi1Si4LaGpET1JBb0wwQVE][Réduction]] et simplification de la quantité de données en
    entrée aux informations les plus relevantes (hit pixels, temps).

  + [[https://indico.in2p3.fr/event/4013/contributions/28939/][Réalisation]] du traitement d’un flux de données série rapide (160 MHz.), en
    provenance de la nouvelle génération de capteurs digitaux (des milliers
    d’images par seconde). A l’appui de la solution COTS FlexRio, basée sur un
    FPGA Virtex V et du standard PXIe, [[https://drive.google.com/open?id=0B5n0QFi1Si4LNjVxT0tDcjNubHc][implémentation]] de la lecture de données
    en DMA vers le PC hôte à travers d’une interface DDR2. [[https://drive.google.com/open?id=0B5n0QFi1Si4LWkp0T1JuUUJaNHc][Développement]] du
    logiciel de contrôle (GUI) sous MATLAB: interface API en C vers le matériel
    et pile TCP/IP java vers le collecteur de données.

- 2002,2007 :: *VHDL/FPGA designer*, /Nuclear Physics Group/, ([[http://www.iphc.cnrs.fr/-DRS-.html][CNRS/IPHC]], Strasbourg)

  + Development of a self contained [[http://www.iphc.cnrs.fr/-tnt-.html][data acquisition]] and online data processing
    system, with low dead time and very high throughput. Responsible for the
    specification and architecture of the embedded VHDL firmware. Conception,
    simulation and implementation of numerical [[https://www.researchgate.net/publication/3139844_TNT_digital_pulse_processor][signal processing]] - digital
    filtering - routines on board and communication with the control software
    via a fast USB2 / Picoblaze interface. Accountable of all the FPGA
    development phases: architecture, documentation, implementation and
    validation/verification. Charged of formulating hardware and development
    timescale estimates. The system operates efficiently in several facilities
    (France, Russia, FInland, Italy, etc.). Achievements reported in “TNT Digital
    Pulse Processor”, presented at the [[https://ieeexplore.ieee.org/document/1644933][IEEE-RT2005]] conference, Stockholm.

  + Numerical [[http://www.iphc.cnrs.fr/-MGS-.html][simulation]], with help of MATLAB, of the behaviour of induced
    signals in new [[https://www.researchgate.net/publication/4098061_A_simple_method_for_the_characterization_of_HPGe_detectors][generation]], segmented HPGe detectors. This software ([[http://www.iphc.cnrs.fr/MGS-Workshop.html][MGS]]) was
    used as a reference within the European collaboration [[https://drive.google.com/open?id=0B5n0QFi1Si4LLWRMRnZtd3h2VjQ][AGATA]], and is
    presented at [[https://ieeexplore.ieee.org/abstract/document/1351438][IEEE-IMTC2004]], Como.

- 2000 :: *Internship*, ([[https://fr.wikipedia.org/wiki/Alcatel-Lucent][Alcatel]], Madrid)

** Education

*** Academic

 - 2013 :: *Master* (90 [[https://diplomeo.com/actualite-credits_ects_european_credits_transfer_system][ECTS]]), /[[http://www.uam.es/ss/Satellite/EscuelaPolitecnica/en/home.htm][Polythecnic University]]/ ([[http://www.uam.es][UAM]] , Madrid), with speciality
   on [[https://gitlab.com/Master-UAM][Embedded Electronics]]

 - 2000 :: *Physical Sciences*, /Option of Electronics/ ([[http://fisicas.ucm.es/][UCM]], Madrid). 5-years
   degree with strong advanced mathematics and physics background, and major
   skills in the field of digital/analog electronics and control systems.

*** [[http://www.mvd-training.com/fr/index.php][Specific]]

 - 2018 (3 dayx) :: *[[https://www.cadence.com/content/cadence-www/global/en_US/home/training/all-courses/85096.html][Tcl Scripting for EDA]]*

 - 2018 (5 days) :: *[[http://www.mvd-training.com/fr/formation/fpga_vhdl/fpga/zynq/EMBD_ZAHS_Zynq_Architecture_Conception_Materielle_Logicielle.php][Zynq™]] All Programmable SoC* Architecture, hardware and
   software conception

 - 2017 (2 days) :: *[[http://www.mvd-training.com/fr/formation/fpga_vhdl/fpga/vivado/FPGA_VSTAXDC_Vivado_Design_Suite_Analyse_statique_de_timing_STA_et_Xilinx_Design_Constraints_XDC.php][MVDS]]* Static Timing Analysis (STA) and Xilinx Design Constraints

 - 2017 (3 days) :: *[[https://www.mvd-training.com/fr/formation/fpga/vivado/][Vivado]] Design Suite* Advanced FPGA conception

 - 2002,2007 :: *Synthesis and simulation with VHDL for FPGA conception*,
   *Implementation with FPGA – Advanced techniques*, *Conception with RocketIO
   Multi Gigabit*, *Virtex PowerPC System Implémentation*

** Languages

 - With *Spanish* as my mother tongue and bilingual in *French*, I have professional
   competence in *English* and elementary *Italian* bases

** Skills

 - Software: matlab / [[https://www.scilab.org/][scilab]] / [[https://www.gnu.org/software/octave/][octave]] / [[http://julialang.org/][julia]], c/c++, [[https://www.python.org/][python]], [[https://es.wikipedia.org/wiki/Emacs_Lisp][elisp]], [[https://www.wikiwand.com/es/Tcl][tcl]]
 - Firmware: VHDL, [[http://www.myhdl.org/][MyHDL]], SystemC, HandelC
 - Specific tools: Xilinx [[https://www.xilinx.com/products/design-tools/ise-design-suite.html][ISE]] / [[https://www.xilinx.com/products/design-tools/vivado.html][Vivado]], Altera Quartus, LabView FPGA
 - Simulation and validation/verification: ModelSim, [[https://ghdl.readthedocs.io][GHDL]]/[[http://gtkwave.sourceforge.net/][GtkWave]], [[https://en.wikipedia.org/wiki/Property_Specification_Language][PSL]], [[https://cocotb.readthedocs.io/en/latest/][cocotb]], [[https://osvvm.org/][osvvm]]
 - [[https://www.gnu.org/software/emacs/][Emacs]] / [[https://git-scm.com/][git]] - GNU/Linux ([[https://www.archlinux.org/][Arch]]) - [[https://awesomewm.org/][Awesome WM]] as preferred working environment

** Several

 - Occasional [[https://csantosb-blogs.gitlab.io/blog-tech/][blogger]]
 - List of publications [[http://www.researchgate.net/profile/Cayetano_Santos][{1}]] [[https://www.mendeley.com/profiles/cayetano-santos][{2}]]
 - [[https://gitlab.com/emacs-elisp][emacs]] et [[https://gitlab.com/aur-packages][arch/aur]] packages
 - [[https://gitlab.com/ip-vhdl][Free]] firmware projects

\\

#+begin_quote
  Madrid, Spain • 44 years-old • [[mailto:cayetano.santos@inventati.org][mail]] \\
   [[https://gitlab.com/users/csantosb/groups][gitlab]] - [[https://github.com/csantosb][github]] - [[https://csantosb.pw/gitea/explore/repos][gitea]] - [[https://www.librecores.org/csantosb][librecores]] \\
   [[https://twitter.com/csantosb][twitter]] [[irc:csantosb][irc]] [[https://diaspora-fr.org/u/csantosb][diaspora]] [[https://lobste.rs/u/csantosb][lobste.rs]] [[https://pinboard.in/u:csantosb][pinboard]] [[https://csantosb-blogs.gitlab.io/blog-tech/][tech blog]] [[https://www.linkedin.com/in/csantosb/][linkedin]]
#+end_quote
--------------
#+begin_quote
  [[https://csantosb.pw/blog/posts/cv-in-org-mode/index.html][about]] - [[https://csantosb-cv.gitlab.io/CV-fr/CV_Cayetano_Santos_fr.pdf][pdf]] [[https://csantosb-cv.gitlab.io/CV-fr/][web]] [[https://csantosb-cv.gitlab.io/CV-fr/CV_Cayetano_Santos_fr.odt][odt]] [[https://csantosb-cv.gitlab.io/CV-fr/CV_Cayetano_Santos_fr.docx][docx]] - [[https://csantosb-cv.gitlab.io/CV-es/][es]] [[https://csantosb-cv.gitlab.io/CV-fr][fr]] [[https://csantosb-cv.gitlab.io/CV-en][en]] - [[https://csantosb.pw/gogs/csantosb-cv/CV-en][source code]]
#+end_quote
